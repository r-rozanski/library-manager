<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        
        <div class="col-md-10 col-md-offset-2 main custom-error-alert">
            <div class="alert alert-danger" role="alert">
                ${errorMessage}
            </div>
        </div>
        
        <div class="col-md-10 col-md-offset-2 text-center">
            <a role="button" class="btn btn-primary" href="<c:url value='/admin' />">Admin Panel</a>
            <a role="button" class="btn btn-primary" href="<c:url value='/admin/author/all' />">All Authors</a>
            <a role="button" class="btn btn-primary" href="<c:url value='/admin/author/new' />">New Author</a>
        </div>
    </div>

</body>
</html>