<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Delete Author
                <a href="<c:url value='/admin/author/all' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <c:choose>
                <c:when test="${!empty author}">
                    <div class="alert alert-info" role="alert">
                        Are you sure you want to delete <strong>${author.name}</strong>?
                    </div>
                    <form:form method="POST" modelAttribute="author">
                        <form:hidden path="name"/>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <a href="<c:url value='/admin/author/all' />" role="button" class="btn btn-primary">
                                    <i class="fa fa-times"></i> Cancel
                                </a>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-trash-o"></i> Delete
                                </button>
                            </div>
                        </div>
                    </form:form>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-danger" role="alert">
                        Author not found.
                    </div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>

</body>
</html>