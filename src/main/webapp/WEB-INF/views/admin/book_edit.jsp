<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="utils" uri="http://www.library-manager.com/utils"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head-select2.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Edit Book
                <a href="<c:url value='/admin/book/all' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <c:choose>
                <c:when test="${!empty book}">
                    <form:form method="POST" modelAttribute="book" class="form-horizontal">
                        <div class="form-group">
                            <label for="title" class="col-md-2 control-label">Title:</label>
                            <div class="col-md-8">
                                <form:input path="title" type="text" class="form-control" id="title" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="title" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="authorsList" class="col-md-2 control-label">Authors:</label>
                            <div class="col-md-8">
                                <form:select path="authors" class="form-control authors-list" id="authors" multiple="multiple">
                                    <c:forEach items="${authorsList}" var="author">
                                        <c:choose>
                                            <c:when test="${utils:contains(book.authors, author)}">
                                                <form:option value="${author.id}" selected="selected">${author.name}</form:option>
                                            </c:when>

                                            <c:otherwise>
                                                <form:option value="${author.id}">${author.name}</form:option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </form:select>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $(".authors-list").select2({
                                            theme: "bootstrap"
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="authors" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="series" class="col-md-2 control-label">Series:</label>
                            <div class="col-md-8">
                                <form:input path="series" type="text" class="form-control" id="series" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="series" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="seriesPart" class="col-md-2 control-label">Series part:</label>
                            <div class="col-md-8">
                                <form:input path="seriesPart" type="number" class="form-control" id="seriesPart" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="seriesPart" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="genre" class="col-md-2 control-label">Genre:</label>
                            <div class="col-md-8">
                                <form:input path="genre" type="text" class="form-control" id="genre" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="genre" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="language" class="col-md-2 control-label">Language:</label>
                            <div class="col-md-8">
                                <form:input path="language" type="text" class="form-control" id="language" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="language" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pages" class="col-md-2 control-label">Pages:</label>
                            <div class="col-md-8">
                                <form:input path="pages" type="number" class="form-control" id="pages" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="pages" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="publisher" class="col-md-2 control-label">Publisher:</label>
                            <div class="col-md-8">
                                <form:input path="publisher" type="text" class="form-control" id="publisher" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="publisher" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="publicationDate" class="col-md-2 control-label">Publication date:</label>
                            <div class="col-md-8">
                                <form:input path="publicationDate" type="text" class="form-control" id="publicationDate" placeholder="dd-mm-yyyy" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="publicationDate" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="isbn" class="col-md-2 control-label">ISBN:</label>
                            <div class="col-md-8">
                                <form:input path="isbn" type="text" class="form-control" id="isbn" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="isbn" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="isbn13" class="col-md-2 control-label">ISBN-13:</label>
                            <div class="col-md-8">
                                <form:input path="isbn13" type="text" class="form-control" id="isbn13" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="isbn13" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-md-2 control-label">Description:</label>
                            <div class="col-md-8">
                                <form:textarea path="description" type="text" class="form-control" id="description" rows="6" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="description" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-8">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o"></i> Save
                                </button>
                            </div>
                        </div>
                    </form:form>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-danger" role="alert">Book not found.</div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>

</body>
</html>