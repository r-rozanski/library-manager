<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Edit User <a href="<c:url value='/admin/user/all' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <c:choose>
                <c:when test="${!empty libraryUser}">
                    <form:form method="POST" modelAttribute="libraryUser" class="form-horizontal">
                        <div class="form-group">
                            <label for="username" class="col-md-2 control-label">Username:</label>
                            <div class="col-md-8">
                                <form:input path="username" type="text" class="form-control" id="username" disabled="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="username" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstName" class="col-md-2 control-label">First name:</label>
                            <div class="col-md-8">
                                <form:input path="firstName" type="text" class="form-control" id="firstName" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="firstName" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastName" class="col-md-2 control-label">Last name:</label>
                            <div class="col-md-8">
                                <form:input path="lastName" type="text" class="form-control" id="lastName" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="lastName" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-2 control-label">Email:</label>
                            <div class="col-md-8">
                                <form:input path="email" type="text" class="form-control" id="email" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="email" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-2 control-label">Phone:</label>
                            <div class="col-md-8">
                                <form:input path="phone" type="text" class="form-control" id="phone" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="phone" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-md-2 control-label">Address:</label>
                            <div class="col-md-8">
                                <form:input path="address" type="text" class="form-control" id="address" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <form:errors path="address" cssClass="custom-form-error" />
                            </div>
                        </div>
                        <form:hidden path="username" />
                        <form:hidden path="password" />
                        <form:hidden path="active" />
                        <form:hidden path="registrationDate" />
                        <form:hidden path="roles" value="ROLE_USER" />
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-8">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o"></i> Save
                                </button>
                            </div>
                        </div>
                    </form:form>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-danger" role="alert">User not found.</div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

</body>
</html>