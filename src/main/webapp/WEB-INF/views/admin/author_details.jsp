<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Author Details <a href="<c:url value='/admin/author/all' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <c:choose>
                <c:when test="${!empty author}">
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Name:</div>
                        <div class="col-md-8">${author.name}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Born:</div>
                        <div class="col-md-8">
                            <joda:format value="${author.dateOfBirth}" style="M-" />
                        </div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Died:</div>
                        <div class="col-md-8">
                            <joda:format value="${author.dateOfDeath}" style="M-" />
                        </div>
                    </div>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-danger" role="alert">Author not found.</div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>

</body>
</html>