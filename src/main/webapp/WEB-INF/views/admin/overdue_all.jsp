<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                All Overdue Books
                <a href="<c:url value='/admin' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>
                            <a href="<c:url value='/admin/book/overdue/all?sort=title' />">
                                Title <c:if test="${ sort == 'title' }"><i class="fa fa-caret-down"></i></c:if>
                            </a>
                        </th>
                        <th class="text-center">
                            <a href="<c:url value='/admin/book/overdue/all?sort=user' />">
                                User <c:if test="${ sort == 'user' }"><i class="fa fa-caret-down"></i></c:if>
                            </a>
                        </th>
                        <th class="text-center">
                            <a href="<c:url value='/admin/book/overdue/all?sort=date' />">
                                Due date <c:if test="${ sort == 'date' }"><i class="fa fa-caret-down"></i></c:if>
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${allOverdueBooks}" var="book" varStatus="status">
                        <tr>
                            <td class="text-center">${status.index + 1}</td>
                            <td>
                                <a href="<c:url value='/admin/book?id=${book.bookId}' />">${book.bookTitle}</a>
                            </td>
                            <td class="text-center">
                                <a href="<c:url value='/admin/user?id=${book.userId}' />">${book.userFullName}</a>
                            </td>
                            <td class="text-center">
                                <joda:format value="${book.dueDate}" style="M-" />
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>