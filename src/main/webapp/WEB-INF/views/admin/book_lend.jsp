<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Lend a Book
                <a href="<c:url value='/admin/user?id=${libraryUserId}' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <form action="<c:url value='/admin/book/lend' />" method="POST" class="form-horizontal">
                <div class="form-group">
                    <label for="book_id" class="col-md-2 control-label">Title:</label>
                    <div class="col-md-8">
                        <select class="form-control" id="book_id" name="book_id">
                            <c:forEach items="${books}" var="book">
                                <option value="${book.id}">${book.title}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <c:if test="${!empty message}">
                            <div class="alert alert-danger" role="alert">${message}</div>
                        </c:if>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="hidden" name="libraryuser_id" value="${libraryUserId}"/>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-8">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-book"></i> Lend
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</body>
</html>