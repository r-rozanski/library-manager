<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                All Authors
                <a href="<c:url value='/admin' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Name</th>
                        <th class="text-center">Date of Birth</th>
                        <th class="text-center">Date of Death</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${authors}" var="author" varStatus="status">
                        <tr>
                            <td class="text-center">${status.index + 1}</td>
                            <td>${author.name}</td>
                            <td class="text-center">
                                <joda:format value="${author.dateOfBirth}" style="M-" />
                            </td>
                            <td class="text-center">
                                <joda:format value="${author.dateOfDeath}" style="M-" />
                            </td>
                            <td class="text-center">
                                <a href="<c:url value='/admin/author?id=${author.id}' />" role="button" class="btn btn-primary">
                                    <i class="fa fa-list"></i> Details
                                </a>
                                <a href="<c:url value='/admin/author/edit?id=${author.id}' />" role="button" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <a href="<c:url value='/admin/author/delete?id=${author.id}' />" role="button" class="btn btn-danger">
                                    <i class="fa fa-trash-o"></i> Delete
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>