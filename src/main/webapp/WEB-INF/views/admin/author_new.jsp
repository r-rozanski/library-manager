<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                New Author
                <a href="<c:url value='/admin' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <form:form method="POST" modelAttribute="author" class="form-horizontal">
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Name:</label>
                    <div class="col-md-8">
                        <form:input path="name" type="text" class="form-control" id="name" placeholder="" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <form:errors path="name" cssClass="custom-form-error" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateOfBirth" class="col-md-2 control-label">Born:</label>
                    <div class="col-md-8">
                        <form:input path="dateOfBirth" type="text" class="form-control" id="dateOfBirth" placeholder="dd-mm-yyyy" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <form:errors path="dateOfBirth" cssClass="custom-form-error" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateOfDeath" class="col-md-2 control-label">Died:</label>
                    <div class="col-md-8">
                        <form:input path="dateOfDeath" type="text" class="form-control" id="dateOfDeath" placeholder="dd-mm-yyyy" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <form:errors path="dateOfDeath" cssClass="custom-form-error" />
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-8">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>

</body>
</html>