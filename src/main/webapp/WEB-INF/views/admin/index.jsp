<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Admin Panel</h1>
            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Books</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="admin-details-list">
                            <li>Available: ${availableBooks}</li>
                            <li>Lent: ${lentBooks}</li>
                            <li>Overdue: ${overdueBooks}</li>
                            <li>Requested: ${requestedBooks}</li>
                            <li><strong>Total: ${allBooks}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/book/new' />">New Book</a>
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/book/all' />">All Books</a>
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/book/request/all' />">Requested books</a>
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/book/overdue/all' />">Overdue books</a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Authors</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="admin-details-list">
                            <li><strong>Total: ${allAuthors}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/author/new' />">New Author</a>
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/author/all' />">All Authors</a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Users</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="admin-details-list">
                            <li>Active: ${activeLibraryUsers}</li>
                            <li>Inactive: ${inactiveLibraryUsers}</li>
                            <li><strong>Total: ${allLibraryUsers}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/user/new' />">New User</a>
                        <a role="button" class="btn btn-sm btn-primary" href="<c:url value='/admin/user/all' />">All Users</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

</body>
</html>