<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                Book Details <a href="<c:url value='/admin/book/all' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <c:choose>
                <c:when test="${!empty book}">
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Title:</div>
                        <div class="col-md-8">${book.title}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Authors:</div>
                        <div class="col-md-8">${book.sortedAuthors}</div>
                    </div>
                    <c:if test="${!empty book.series}">
                        <div class="row admin-details-row">
                            <div class="col-md-2 text-right text-bold">Series:</div>
                            <div class="col-md-8">${book.series} #${book.seriesPart}</div>
                        </div>
                    </c:if>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Genre:</div>
                        <div class="col-md-8">${book.genre}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Language:</div>
                        <div class="col-md-8">${book.language}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Pages:</div>
                        <div class="col-md-8">${book.pages}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Publisher:</div>
                        <div class="col-md-8">${book.publisher}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">Publication date:</div>
                        <div class="col-md-8">
                            <joda:format value="${book.publicationDate}" style="M-" />
                        </div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">ISBN:</div>
                        <div class="col-md-8">${book.isbn}</div>
                    </div>
                    <div class="row admin-details-row">
                        <div class="col-md-2 text-right text-bold">ISBN-13:</div>
                        <div class="col-md-8">${book.isbn13}</div>
                    </div>
                    <div class="row admin-book-description">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-justify">${book.description}</div>
                    </div>
                </c:when>

                <c:otherwise>
                    <div class="alert alert-danger" role="alert">Book not found.</div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>

</body>
</html>