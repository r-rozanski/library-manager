<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="../partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="../partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="../partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">
                All Users
                <a href="<c:url value='/admin' />" role="button" class="btn btn-primary pull-right">Go Back</a>
            </h1>

            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Username</th>
                        <th class="text-center">Full Name</th>
                        <th class="text-center">Roles</th>
                        <th class="text-center">Actions</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${libraryUsers}" var="libraryUser" varStatus="status">
                        <tr>
                            <td class="text-center">${status.index + 1}</td>
                            <td>${libraryUser.username}</td>
                            <td class="text-center">${libraryUser.firstName} ${libraryUser.lastName}</td>
                            <td class="text-center">${libraryUser.formattedRoles}</td>
                            <td class="text-center">
                                    <a href="<c:url value='/admin/user?id=${libraryUser.id}' />" role="button" class="btn btn-primary">
                                        <i class="fa fa-list"></i> Details
                                    </a>
                                    <c:if test="${!libraryUser.admin}">
                                        <a href="<c:url value='/admin/user/edit?id=${libraryUser.id}' />" role="button" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                        
                                        <c:choose>
                                            <c:when test="${libraryUser.active}">
                                                <a href="<c:url value='/admin/user/disable?id=${libraryUser.id}' />" role="button" class="btn btn-primary">
                                                    <i class="fa fa-user"></i> Disable
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value='/admin/user/enable?id=${libraryUser.id}' />" role="button" class="btn btn-primary">
                                                    <i class="fa fa-user"></i> Enable
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                        
                                        
                                        <a href="<c:url value='/admin/user/disable?id=${libraryUser.id}' />" role="button" class="btn btn-primary">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </c:if>
                            </td>
                            <td class="text-center">
                                <c:choose>
                                    <c:when test="${libraryUser.active}">
                                        <i class="fa fa-check color-green"></i>
                                    </c:when>
                                    
                                    <c:otherwise>
                                        <i class="fa fa-times color-red"></i>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>