<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value='/' />"><i class="fa fa-book"></i> Library Manager</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <sec:authorize access="!isAuthenticated()">
                <form class="navbar-form navbar-right">
                    <a href="<c:url value='/login' />" class="btn btn-primary account-button">
                        <i class="fa fa-sign-in"></i> Log in
                    </a>
                </form>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <form action="<c:url value='/logout' />" method="post" id="logoutForm" class="navbar-form navbar-right">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <a href="<c:url value='/account' />" class="btn btn-default account-button">
                        <i class="fa fa-user"></i> Account
                    </a>
                    <button type="submit" class="btn btn-primary account-button">
                        <i class="fa fa-sign-out"></i> Log out
                    </button>
                </form>
            </sec:authorize>

        </div>
    </div>
</nav>