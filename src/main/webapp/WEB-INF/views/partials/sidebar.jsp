<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<form action="<c:url value='/search' />" method="get" class="quick-search-form">
    <div class="form-group">
        <input name="title" type="text" class="form-control" placeholder="Search by title...">
    </div>
</form>

<ul class="nav nav-sidebar">
    <li><a href="<c:url value='/search/advanced' />">Advanced Search</a></li>
    <li><a href="catalog.html">Library Catalog</a></li>
    <li><a href="news.html">News and Events</a></li>
    <li><a href="hours.html">Opening Hours</a></li>
    <li><a href="contact.html">Contact Us</a></li>
</ul>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <a role="button" class="btn btn-default btn-block" href="<c:url value='/admin' />">Admin Panel</a>
</sec:authorize>