<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="UTF-8">
<title>Library Manager</title>
<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/select2.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/select2-bootstrap.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
<script type="text/javascript" src="<c:url value='/static/js/jquery.js' />"></script>
<script type="text/javascript" src="<c:url value='/static/js/bootstrap.js' />"></script>
<script type="text/javascript" src="<c:url value='/static/js/select2.js' />"></script>