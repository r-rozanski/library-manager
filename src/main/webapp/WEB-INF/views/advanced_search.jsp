<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Advanced Search</h1>

            <form:form method="POST" modelAttribute="searchCriteria" class="form-horizontal">
                <div class="form-group">
                    <label for="phrase" class="col-md-1 col-md-offset-1 control-label">Search:</label>
                    <div class="col-md-4">
                        <form:input path="phrase" type="text" class="form-control" id="phrase" placeholder="" />
                    </div>
                    <div class="col-md-1 text-center">
                        <label class="control-label">in</label>
                    </div>
                    <div class="col-md-2">
                        <form:select path="category" class="form-control" items="${searchCategories}" style="text-transform: capitalize;" />
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary" style="width: 75px;">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>

            </form:form>
            
            <c:if test="${!empty message}">
                <div class="alert alert-danger" role="alert" style="margin-top: 100px;">${message}</div>
            </c:if>
            
            <c:if test="${!empty books}">
            <table class="table" style="margin-top: 100px;">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Title</th>
                        <th class="text-center">Authors</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${books}" var="book" varStatus="status">
                        <tr>
                            <td class="text-center">${status.index + 1}</td>
                            <td>${book.title}</td>
                            <td class="text-center">${book.sortedAuthors}</td>
                            <td class="text-center">
                                <a href="<c:url value='/book?id=${book.id}' />" role="button" class="btn btn-primary">
                                    <i class="fa fa-list"></i> Details</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            </c:if>

        </div>
    </div>

</body>
</html>