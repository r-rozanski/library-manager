<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Account</h1>
            
            <div class="col-md-6 col-md-offset-3">

                <form name="loginForm" action="<c:url value='/login' />" method="POST" class="form-horizontal">
                    <c:if test="${!empty error}">
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="alert alert-danger" role="alert">${error}</div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${!empty message}">
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="alert alert-success" role="alert">${message}</div>
                            </div>
                        </div>
                    </c:if>
                    <div class="form-group">
                        <label for="username" class="col-md-2 control-label">Username:</label>
                        <div class="col-md-8">
                            <input type="text" name="username" id="username" class="form-control" autofocus="autofocus" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-2 control-label">Password:</label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password" class="form-control" />
                        </div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <button type="submit" name="submit" class="btn btn-primary">
                                <i class="fa fa-sign-in"></i> Log in
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</body>
</html>