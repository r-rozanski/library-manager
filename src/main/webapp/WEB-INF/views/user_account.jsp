<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Account</h1>
            
            <c:if test="${!libraryUser.active}">
                <div class="alert alert-danger" role="alert">Your account is disabled.</div>
            </c:if>

            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Username:</div>
                <div class="col-md-8">${libraryUser.username}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">First name:</div>
                <div class="col-md-8">${libraryUser.firstName}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Last name:</div>
                <div class="col-md-8">${libraryUser.lastName}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Email:</div>
                <div class="col-md-8">${libraryUser.email}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Phone:</div>
                <div class="col-md-8">${libraryUser.phone}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Address:</div>
                <div class="col-md-8">${libraryUser.address}</div>
            </div>
            <div class="row admin-details-row">
                <div class="col-md-2 text-right text-bold">Registered:</div>
                <div class="col-md-8">
                    <joda:format value="${libraryUser.registrationDate}" style="M-" />
                </div>
            </div>
            
            <div class="row admin-details-row books-list-account">
                <div class="col-md-2 text-right text-bold">Borrowed books:</div>
                <div class="col-md-8">
                    <c:if test="${!empty borrowedBooks}">
                        <ol class="account-list">
                            <c:forEach items="${borrowedBooks}" var="book">
                                <li>
                                    <c:if test="${book.overdue}">
                                        <span class="color-red"><strong>OVERDUE</strong></span>
                                    </c:if>
                                    <a href="<c:url value='/book?id=${book.bookId}' />">${book.bookTitle}</a> (<joda:format value="${book.loanStart}" style="M-" /> - <joda:format value="${book.dueDate}" style="M-" />)
                                </li>
                            </c:forEach>
                        </ol>
                    </c:if>
                </div>
            </div>

            <div class="row admin-details-row books-list-account">
                <div class="col-md-2 text-right text-bold">Requested books:</div>
                <div class="col-md-8">
                    <c:if test="${!empty requestedBooks}">
                        <ol class="account-list">
                            <c:forEach items="${requestedBooks}" var="book">
                                <li><a href="<c:url value='/book?id=${book.bookId}' />">${book.bookTitle}</a> (<joda:format value="${book.requestDate}" style="M-" />)</li>
                            </c:forEach>
                        </ol>
                    </c:if>
                </div>
            </div>

        </div>
    </div>

</body>
</html>