<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="en">
<jsp:include page="partials/head.jsp"></jsp:include>
</head>
<body>

    <jsp:include page="partials/navbar.jsp"></jsp:include>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 sidebar">
                <jsp:include page="partials/sidebar.jsp"></jsp:include>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Search</h1>

            <c:choose>
                <c:when test="${!empty message}">
                    <div class="alert alert-danger" role="alert">${message}</div>
                </c:when>

                <c:otherwise>
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Title</th>
                                <th class="text-center">Authors</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${books}" var="book" varStatus="status">
                                <tr>
                                    <td class="text-center">${status.index + 1}</td>
                                    <td>${book.title}</td>
                                    <td class="text-center">${book.sortedAuthors}</td>
                                    <td class="text-center">
                                        <a href="<c:url value='/book?id=${book.id}' />" role="button" class="btn btn-primary">
                                            <i class="fa fa-list"></i> Details
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>

        </div>
    </div>

</body>
</html>