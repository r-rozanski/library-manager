package eu.rrozanski.librarymanager.dao;

import java.util.List;

import eu.rrozanski.librarymanager.model.Author;

public interface AuthorDao {

    void saveAuthor(Author author);

    void updateAuthor(Author author);

    void deleteAuthor(Author author);

    List<Author> findAllAuthors();

    Author findAuthorById(int id);

    public Boolean checkIfAuthorExists(String name);

    Long countAll();

}
