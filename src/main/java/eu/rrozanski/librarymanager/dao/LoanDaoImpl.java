package eu.rrozanski.librarymanager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.Loan;

@Repository("loanDao")
public class LoanDaoImpl extends AbstractDao implements LoanDao {

    public void saveLoan(Loan loan) {
        persist(loan);
    }

    public void deleteLoan(Loan loan) {
        delete(loan);
    }

    public void renewBook(Loan loan) {
        Query query = getSession().createQuery("update Loan set dueDate = :dueDate, renewed = :renewed where id = :id");
        query.setParameter("dueDate", loan.getDueDate());
        query.setParameter("renewed", loan.getRenewed());
        query.setParameter("id", loan.getId());
        query.executeUpdate();
    }

    public Loan findById(int id) {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.eq("id", id));
        return (Loan) criteria.uniqueResult();
    }

    public Loan findByBookId(int bookId) {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.eq("bookId", bookId));
        return (Loan) criteria.uniqueResult();
    }

    public List<Loan> findByUserId(int userId) {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.eq("userId", userId));
        criteria.addOrder(Order.asc("bookTitle"));
        @SuppressWarnings("unchecked")
        List<Loan> result = criteria.list();
        return result;
    }

    public Loan findByBookAndUser(int bookId, int userId) {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.and(Restrictions.eq("bookId", bookId), Restrictions.eq("userId", userId)));
        return (Loan) criteria.uniqueResult();
    }

    public List<Loan> findAll() {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.addOrder(Order.asc("bookTitle"));
        @SuppressWarnings("unchecked")
        List<Loan> result = criteria.list();
        return result;
    }

    public List<Loan> findAllOverdue(LoanSortingOrder sortingOrder) {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.lt("dueDate", LocalDate.now()));

        switch (sortingOrder) {
        case TITLE:
            criteria.addOrder(Order.asc("bookTitle"));
            break;
        case USER:
            criteria.addOrder(Order.asc("userFullName"));
            break;
        case DATE:
            criteria.addOrder(Order.asc("dueDate"));
            break;
        }

        @SuppressWarnings("unchecked")
        List<Loan> result = criteria.list();
        return result;
    }

    public Long countAll() {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    public Long countOverdue() {
        Criteria criteria = getSession().createCriteria(Loan.class);
        criteria.add(Restrictions.lt("dueDate", LocalDate.now()));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
