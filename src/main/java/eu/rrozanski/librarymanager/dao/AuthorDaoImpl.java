package eu.rrozanski.librarymanager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.Author;

@Repository("authorDao")
public class AuthorDaoImpl extends AbstractDao implements AuthorDao {

    public void saveAuthor(Author author) {
        persist(author);
    }

    public void updateAuthor(Author author) {
        update(author);
    }

    public void deleteAuthor(Author author) {
        delete(author);
    }

    @SuppressWarnings("unchecked")
    public List<Author> findAllAuthors() {
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.addOrder(Order.asc("name"));
        return (List<Author>) criteria.list();
    }

    public Author findAuthorById(int id) {
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.add(Restrictions.eq("id", id));
        return (Author) criteria.uniqueResult();
    }

    public Boolean checkIfAuthorExists(String name) {
        Boolean authorExists;
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.add(Restrictions.eq("name", name));
        if (criteria.list().isEmpty()) {
            authorExists = false;
        } else {
            authorExists = true;
        }
        return authorExists;
    }

    public Long countAll() {
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
