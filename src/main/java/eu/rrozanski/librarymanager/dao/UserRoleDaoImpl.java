package eu.rrozanski.librarymanager.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.UserRole;

@Repository("userRoleDao")
public class UserRoleDaoImpl extends AbstractDao implements UserRoleDao {

    public UserRole findByRoleName(String role) {
        Criteria criteria = getSession().createCriteria(UserRole.class);
        criteria.add(Restrictions.eq("role", role));
        return (UserRole) criteria.uniqueResult();
    }

}
