package eu.rrozanski.librarymanager.dao;

import java.util.List;

import eu.rrozanski.librarymanager.model.RequestedBook;

public interface RequestedBookDao {

    public enum RequestedBookSortingOrder { TITLE, USER, DATE };

    void saveRequestedBook(RequestedBook requestedBook);

    void deleteRequestedBook(RequestedBook requestedBook);

    boolean checkIfBookRequested(int bookId, int userId);

    RequestedBook findById(int id);

    RequestedBook findByBookAndUser(int bookId, int userId);

    List<RequestedBook> findByBookId(int bookId);

    List<RequestedBook> findAll(RequestedBookSortingOrder sortingOrder);

    List<RequestedBook> findAllByUser(int userId);

}
