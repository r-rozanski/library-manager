package eu.rrozanski.librarymanager.dao;

import eu.rrozanski.librarymanager.model.UserRole;

public interface UserRoleDao {

    public UserRole findByRoleName(String role);

}
