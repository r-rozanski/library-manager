package eu.rrozanski.librarymanager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.RequestedBook;

@Repository("requestedBookDao")
public class RequestedBookDaoImpl extends AbstractDao implements RequestedBookDao {

    public void saveRequestedBook(RequestedBook requestedBook) {
        persist(requestedBook);
    }

    public void deleteRequestedBook(RequestedBook requestedBook) {
        delete(requestedBook);
    }

    public boolean checkIfBookRequested(int bookId, int userId) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);
        criteria.add(Restrictions.and(Restrictions.eq("bookId", bookId), Restrictions.eq("userId", userId)));
        return !(criteria.list().isEmpty());
    }

    public RequestedBook findById(int id) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);
        criteria.add(Restrictions.eq("id", id));
        return (RequestedBook) criteria.uniqueResult();
    }

    public RequestedBook findByBookAndUser(int bookId, int userId) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);
        criteria.add(Restrictions.and(Restrictions.eq("bookId", bookId), Restrictions.eq("userId", userId)));
        return (RequestedBook) criteria.uniqueResult();
    }

    public List<RequestedBook> findByBookId(int bookId) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);
        criteria.add(Restrictions.eq("bookId", bookId));
        criteria.addOrder(Order.asc("bookTitle"));
        @SuppressWarnings("unchecked")
        List<RequestedBook> result = criteria.list();
        return result;
    }

    public List<RequestedBook> findAll(RequestedBookSortingOrder sortingOrder) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);

        switch (sortingOrder) {
        case TITLE:
            criteria.addOrder(Order.asc("bookTitle"));
            break;
        case USER:
            criteria.addOrder(Order.asc("userFullName"));
            break;
        case DATE:
            criteria.addOrder(Order.asc("requestDate"));
            break;
        }

        @SuppressWarnings("unchecked")
        List<RequestedBook> result = criteria.list();
        return result;
    }

    public List<RequestedBook> findAllByUser(int userId) {
        Criteria criteria = getSession().createCriteria(RequestedBook.class);
        criteria.add(Restrictions.eq("userId", userId));
        criteria.addOrder(Order.asc("bookTitle"));
        @SuppressWarnings("unchecked")
        List<RequestedBook> result = criteria.list();
        return result;
    }

}
