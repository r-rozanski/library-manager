package eu.rrozanski.librarymanager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.LibraryUser;

@Repository("libraryUserDao")
public class LibraryUserDaoImpl extends AbstractDao implements LibraryUserDao {

    public void saveUser(LibraryUser libraryUser) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String plainPassword = libraryUser.getPassword();
        libraryUser.setPassword(encoder.encode(plainPassword));
        persist(libraryUser);
    }

    public void updateUser(LibraryUser libraryUser) {
        Query query = getSession().createQuery("update LibraryUser set "
                + "firstName = :firstName, lastName = :lastName, "
                + "email = :email, phone = :phone, address = :address "
                + "where id = :id");
        query.setParameter("firstName", libraryUser.getFirstName());
        query.setParameter("lastName", libraryUser.getLastName());
        query.setParameter("email", libraryUser.getEmail());
        query.setParameter("phone", libraryUser.getPhone());
        query.setParameter("address", libraryUser.getAddress());
        query.setParameter("id", libraryUser.getId());
        query.executeUpdate();
    }

    public void deleteUser(LibraryUser libraryUser) {
        delete(libraryUser);
    }

    public void changeStatus(int userId, boolean status) {
        Query query = getSession().createQuery("update LibraryUser set active = :active where id = :id");
        query.setParameter("active", status);
        query.setParameter("id", userId);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<LibraryUser> findAllUsers() {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.asc("username"));
        return (List<LibraryUser>) criteria.list();
    }

    public LibraryUser findByUsername(String username) {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.add(Restrictions.eq("username", username));
        return (LibraryUser) criteria.uniqueResult();
    }

    public LibraryUser findById(int id) {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.add(Restrictions.eq("id", id));
        return (LibraryUser) criteria.uniqueResult();
    }

    public Boolean checkIfUserExists(String username) {
        Boolean userExists;
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.add(Restrictions.eq("username", username));
        if (criteria.list().isEmpty()) {
            userExists = false;
        } else {
            userExists = true;
        }
        return userExists;
    }

    public Long countAll() {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    public Long countActive() {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.add(Restrictions.eq("active", true));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    public Long countInactive() {
        Criteria criteria = getSession().createCriteria(LibraryUser.class);
        criteria.add(Restrictions.eq("active", false));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
