package eu.rrozanski.librarymanager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import eu.rrozanski.librarymanager.model.Book;

@Repository("bookDao")
public class BookDaoImpl extends AbstractDao implements BookDao {

    public void saveBook(Book book) {
        persist(book);
    }

    public void updateBook(Book book) {
        update(book);
    }

    public void deleteBook(Book book) {
        delete(book);
    }

    @SuppressWarnings("unchecked")
    public List<Book> findAllBooks() {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.asc("title"));
        return (List<Book>) criteria.list();
    }

    public Book findBookById(int id) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.add(Restrictions.eq("id", id));
        return (Book) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findBooksByTitle(String title) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.ilike("title", title, MatchMode.ANYWHERE));
        criteria.addOrder(Order.asc("title"));
        return (List<Book>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findByAuthorId(int authorId) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.createAlias("authors", "aut", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("aut.id", authorId));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (List<Book>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findByAuthorName(String name) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.createAlias("authors", "aut", JoinType.FULL_JOIN);
        criteria.add(Restrictions.ilike("aut.name", name, MatchMode.ANYWHERE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (List<Book>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findBySeries(String series) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.ilike("series", series, MatchMode.ANYWHERE));
        criteria.addOrder(Order.asc("title"));
        return (List<Book>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findByIsbn(IsbnType type, String number) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        switch (type) {
        case ISBN10:
            criteria.add(Restrictions.ilike("isbn", number, MatchMode.ANYWHERE));
            break;
        case ISBN13:
            criteria.add(Restrictions.ilike("isbn13", number, MatchMode.ANYWHERE));
            break;
        }

        criteria.addOrder(Order.asc("title"));
        return (List<Book>) criteria.list();
    }

    public Boolean checkIfBookExists(String title) {
        Boolean bookExists;
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.add(Restrictions.eq("title", title));
        if (criteria.list().isEmpty()) {
            bookExists = false;
        } else {
            bookExists = true;
        }
        return bookExists;
    }

    public Long countAll() {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
