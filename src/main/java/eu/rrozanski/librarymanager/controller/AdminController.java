package eu.rrozanski.librarymanager.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.rrozanski.librarymanager.dao.LoanDao.LoanSortingOrder;
import eu.rrozanski.librarymanager.dao.RequestedBookDao.RequestedBookSortingOrder;
import eu.rrozanski.librarymanager.model.Author;
import eu.rrozanski.librarymanager.model.Book;
import eu.rrozanski.librarymanager.model.LibraryUser;
import eu.rrozanski.librarymanager.model.Loan;
import eu.rrozanski.librarymanager.model.RequestedBook;
import eu.rrozanski.librarymanager.service.AuthorService;
import eu.rrozanski.librarymanager.service.BookService;
import eu.rrozanski.librarymanager.service.LibraryUserService;
import eu.rrozanski.librarymanager.service.LoanService;
import eu.rrozanski.librarymanager.service.RequestedBookService;
import eu.rrozanski.librarymanager.service.UserRoleService;

@Controller
@RequestMapping("/")
public class AdminController {

    @Autowired
    AuthorService authorService;

    @Autowired
    BookService bookService;

    @Autowired
    LibraryUserService libraryUserService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    RequestedBookService requestedBookService;

    @Autowired
    LoanService loanService;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminHome(ModelMap model) {
        Long availableBooks = bookService.countAvailable();
        Long lentBooks = loanService.countAll();
        Long overdueBooks = loanService.countOverdue();
        Integer requestedBooks = requestedBookService.countAll();
        Long allBooks = bookService.countAll();
        Long allAuthors = authorService.countAll();
        Long activeLibraryUsers = libraryUserService.countActive();
        Long inactiveLibraryUsers = libraryUserService.countInactive();
        Long allLibraryUsers = libraryUserService.countAll();

        model.addAttribute("availableBooks", availableBooks);
        model.addAttribute("lentBooks", lentBooks);
        model.addAttribute("overdueBooks", overdueBooks);
        model.addAttribute("requestedBooks", requestedBooks);
        model.addAttribute("allBooks", allBooks);
        model.addAttribute("allAuthors", allAuthors);
        model.addAttribute("activeLibraryUsers", activeLibraryUsers);
        model.addAttribute("inactiveLibraryUsers", inactiveLibraryUsers);
        model.addAttribute("allLibraryUsers", allLibraryUsers);

        return "admin/index";

    }

    @RequestMapping(value = "/admin/author/all", method = RequestMethod.GET)
    public String adminAllAuthors(ModelMap model) {
        List<Author> authors = authorService.findAllAuthors();
        model.addAttribute("authors", authors);
        return "admin/author_all";
    }

    @RequestMapping(value = "/admin/author", method = RequestMethod.GET)
    public String adminAuthorDetails(@RequestParam("id") String authorId,
            ModelMap model) {
        Author author = authorService
                .findAuthorById(Integer.parseInt(authorId));
        model.addAttribute("author", author);
        return "admin/author_details";
    }

    @RequestMapping(value = "/admin/author/new", method = RequestMethod.GET)
    public String adminNewAuthor(ModelMap model) {
        Author author = new Author();
        model.addAttribute("author", author);
        return "admin/author_new";
    }

    @RequestMapping(value = "/admin/author/new", method = RequestMethod.POST)
    public String adminSaveAuthor(@Valid Author author, BindingResult result,
            ModelMap model) {

        if (result.hasErrors()) {
            return "admin/author_new";
        }

        if ((author.getDateOfDeath() != null)
                && (author.getDateOfDeath().isBefore(author.getDateOfBirth()))) {
            result.rejectValue("dateOfDeath", "error.author",
                    "Author cannot die before being born.");
            return "admin/author_new";
        }

        if (authorService.checkIfAuthorExists(author.getName())) {
            result.rejectValue("name", "error.author", "Author already exists.");
            return "admin/author_new";
        } else {
            authorService.saveAuthor(author);
        }

        model.addAttribute("successMessage",
                "Author <strong>" + author.getName() + "</strong> added.");

        return "admin/author_success";
    }

    @RequestMapping(value = "/admin/author/edit", method = RequestMethod.GET)
    public String adminEditAuthor(@RequestParam("id") String authorId,
            ModelMap model) {
        Author author = authorService
                .findAuthorById(Integer.parseInt(authorId));
        model.addAttribute("author", author);
        return "admin/author_edit";
    }

    @RequestMapping(value = "/admin/author/edit", method = RequestMethod.POST)
    public String adminUpdateAuthor(
            @Valid @ModelAttribute("author") Author author,
            BindingResult result, ModelMap model)
            throws DataIntegrityViolationException {

        if (result.hasErrors()) {
            return "admin/author_edit";
        }

        if ((author.getDateOfDeath() != null)
                && (author.getDateOfDeath().isBefore(author.getDateOfBirth()))) {
            result.rejectValue("dateOfDeath", "error.author",
                    "Author cannot die before being born.");
            return "admin/author_new";
        }

        try {
            authorService.updateAuthor(author);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                result.rejectValue("name", "error.author",
                        "Author already exists.");
            } else {
                result.rejectValue("name", "error.author",
                        "Data integrity error.");
            }

            return "admin/author_edit";
        }

        model.addAttribute("successMessage",
                "Author <strong>" + author.getName() + "</strong> updated.");

        return "admin/author_success";
    }

    @RequestMapping(value = "/admin/author/delete", method = RequestMethod.GET)
    public String adminAskIfDeleteAuthor(@RequestParam("id") String authorId,
            ModelMap model) {
        Author author = authorService
                .findAuthorById(Integer.parseInt(authorId));
        model.addAttribute("author", author);
        return "admin/author_delete";
    }

    @RequestMapping(value = "/admin/author/delete", method = RequestMethod.POST)
    public String adminDeleteAuthor(@ModelAttribute("author") Author author,
            BindingResult result, ModelMap model) {
        String deletedAuthorName = author.getName();
        List<Book> books = bookService.findByAuthorId(author.getId());
        String path = null;

        if (books.isEmpty()) {
            authorService.deleteAuthor(author);
            model.addAttribute("successMessage", "Author <strong>"
                    + deletedAuthorName + "</strong> deleted.");
            path = "admin/author_success";
        } else {
            model.addAttribute("errorMessage", "Author <strong>"
                    + deletedAuthorName + "</strong> can't be deleted. "
                    + "There is still at least one book by this author "
                    + "in the database.");
            path = "admin/author_error";
        }

        return path;
    }

    @RequestMapping(value = "/admin/book/all", method = RequestMethod.GET)
    public String adminAllBooks(ModelMap model) {
        List<Book> books = bookService.findAllBooks();
        model.addAttribute("books", books);
        return "admin/book_all";
    }

    @RequestMapping(value = "/admin/book", method = RequestMethod.GET)
    public String adminBookDetails(@RequestParam("id") String bookId,
            ModelMap model) {

        Book book = bookService.findBookById(Integer.parseInt(bookId));
        List<String> sortedAuthorsNames = new ArrayList<String>();

        if (book != null) {
            for (Author author : book.getAuthors()) {
                sortedAuthorsNames.add(author.getName());
            }
            Collections.sort(sortedAuthorsNames);
        }

        model.addAttribute("book", book);
        model.addAttribute("sortedAuthorsNames", sortedAuthorsNames);
        return "admin/book_details";
    }

    @ModelAttribute("authorsList")
    public List<Author> populateAuthors() {
        List<Author> authorsList = authorService.findAllAuthors();
        return authorsList;
    }

    @RequestMapping(value = "/admin/book/new", method = RequestMethod.GET)
    public String adminNewBook(ModelMap model) {
        Book book = new Book();
        model.addAttribute("book", book);
        return "admin/book_new";
    }

    @RequestMapping(value = "/admin/book/new", method = RequestMethod.POST)
    public String adminSaveAuthor(@ModelAttribute("book") @Valid Book book,
            BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "admin/book_new";
        }

        if ((book.getPublicationDate().isAfter(new LocalDate()))) {
            result.rejectValue("publicationDate", "error.book",
                    "Publication date must be in the past.");
            return "admin/book_new";
        }

        if (bookService.checkIfBookExists(book.getTitle())) {
            result.rejectValue("title", "error.book", "Book already exists.");
            return "admin/book_new";
        } else {
            bookService.saveBook(book);
        }

        model.addAttribute("successMessage", "Book <strong>" + book.getTitle()
                + "</strong> added.");

        return "admin/book_success";
    }

    @RequestMapping(value = "/admin/book/edit", method = RequestMethod.GET)
    public String adminEditBook(@RequestParam("id") String bookId,
            ModelMap model) {
        Book book = bookService.findBookById(Integer.parseInt(bookId));
        model.addAttribute("book", book);
        return "admin/book_edit";
    }

    @RequestMapping(value = "/admin/book/edit", method = RequestMethod.POST)
    public String adminUpdateBook(@Valid @ModelAttribute("book") Book book,
            BindingResult result, ModelMap model)
            throws DataIntegrityViolationException {

        if (result.hasErrors()) {
            return "admin/book_edit";
        }

        if ((book.getPublicationDate().isAfter(new LocalDate()))) {
            result.rejectValue("publicationDate", "error.book",
                    "Publication date must be in the past.");
            return "admin/book_new";
        }

        try {
            bookService.updateBook(book);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                result.rejectValue("title", "error.book",
                        "Book already exists.");
            } else {
                result.rejectValue("title", "error.book",
                        "Data integrity error.");
            }

            return "admin/book_edit";
        }

        model.addAttribute("successMessage", "Book <strong>" + book.getTitle()
                + "</strong> updated.");

        return "admin/book_success";

    }

    @RequestMapping(value = "/admin/book/delete", method = RequestMethod.GET)
    public String adminAskIfDeleteBook(@RequestParam("id") String bookId,
            ModelMap model) {
        Book book = bookService.findBookById(Integer.parseInt(bookId));
        model.addAttribute("book", book);
        return "admin/book_delete";
    }

    @RequestMapping(value = "/admin/book/delete", method = RequestMethod.POST)
    public String adminDeleteBook(@ModelAttribute("book") Book book,
            BindingResult result, ModelMap model) {
        String deletedBookTitleName = book.getTitle();

        bookService.deleteBook(book);

        model.addAttribute("successMessage", "Book <strong>"
                + deletedBookTitleName + "</strong> deleted.");

        return "admin/book_success";
    }

    @RequestMapping(value = "/admin/user/all", method = RequestMethod.GET)
    public String adminAllUsers(ModelMap model) {

        List<LibraryUser> libraryUsers = libraryUserService.findAllUsers();
        model.addAttribute("libraryUsers", libraryUsers);

        return "admin/user_all";
    }

    @RequestMapping(value = "/admin/user", method = RequestMethod.GET)
    public String adminUserDetails(@RequestParam("id") String userId,
            ModelMap model) {

        LibraryUser libraryUser = libraryUserService.findById(Integer.parseInt(userId));
        List<RequestedBook> requestedBooks = requestedBookService.findAllByUser(libraryUser.getId());
        List<Loan> borrowedBooks = loanService.findByUserId(libraryUser.getId());

        model.addAttribute("libraryUser", libraryUser);
        model.addAttribute("requestedBooks", requestedBooks);
        model.addAttribute("borrowedBooks", borrowedBooks);

        return "admin/user_details";
    }

    @RequestMapping(value = "/admin/user/new", method = RequestMethod.GET)
    public String adminNewUser(ModelMap model) {

        LocalDate today = LocalDate.now();
        LibraryUser libraryUser = new LibraryUser();

        libraryUser.setActive(true);
        libraryUser.setRegistrationDate(today);
        model.addAttribute("libraryUser", libraryUser);

        return "admin/user_new";
    }

    @RequestMapping(value = "/admin/user/new", method = RequestMethod.POST)
    public String adminSaveUser(
            @Valid @ModelAttribute("libraryUser") LibraryUser libraryUser,
            BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "admin/user_new";
        }

        if (libraryUser.getPassword().isEmpty()) {
            result.rejectValue("password", "error.libraryUser",
                    "Password is required.");
            return "admin/user_new";
        }

        if (libraryUserService.checkIfUserExists(libraryUser.getUsername())) {
            result.rejectValue("username", "error.libraryUser",
                    "User already exists.");
            return "admin/user_new";
        } else {
            libraryUserService.saveUser(libraryUser);
        }

        model.addAttribute("successMessage",
                "User <strong>" + libraryUser.getUsername()
                        + "</strong> added.");

        return "admin/user_success";
    }

    @RequestMapping(value = "/admin/user/edit", method = RequestMethod.GET)
    public String adminEditUser(@RequestParam("id") String userId,
            ModelMap model) {

        LibraryUser libraryUser = libraryUserService.findById(Integer
                .parseInt(userId));
        libraryUser.setPassword("password");

        model.addAttribute("libraryUser", libraryUser);

        return "admin/user_edit";
    }

    @RequestMapping(value = "/admin/user/edit", method = RequestMethod.POST)
    public String adminUpdateUser(
            @Valid @ModelAttribute("libraryUser") LibraryUser libraryUser,
            BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "admin/user_edit";
        }

        libraryUserService.updateUser(libraryUser);

        model.addAttribute("successMessage",
                "User <strong>" + libraryUser.getUsername()
                        + "</strong> updated.");

        return "admin/user_success";
    }

    @RequestMapping(value = "/admin/user/delete", method = RequestMethod.GET)
    public String adminAskIfDeleteUser(@RequestParam("id") String userId,
            ModelMap model) {

        LibraryUser libraryUser = libraryUserService
                .findById(Integer.parseInt(userId));

        model.addAttribute("libraryUser", libraryUser);

        return "admin/user_delete";
    }

    @RequestMapping(value = "/admin/user/delete", method = RequestMethod.POST)
    public String adminDeleteUser(@ModelAttribute("libraryUser") LibraryUser libraryUser,
            BindingResult result, ModelMap model) {

        String deletedUserUsername = libraryUser.getUsername();

        libraryUserService.deleteUser(libraryUser);

        model.addAttribute("successMessage", "User <strong>"
                + deletedUserUsername + "</strong> deleted.");

        return "admin/user_success";
    }

    @RequestMapping(value = "/admin/book/request/all", method = RequestMethod.GET)
    public String adminAllRequestedBooks(@RequestParam(value = "sort", required = false) String sort,
            ModelMap model) {

        List<RequestedBook> allRequestedBooks;
        if (sort == null) sort = "title";

        switch (sort) {
        case "title":
            allRequestedBooks = requestedBookService.findAll(RequestedBookSortingOrder.TITLE);
            break;
        case "user":
            allRequestedBooks = requestedBookService.findAll(RequestedBookSortingOrder.USER);
            break;
        case "date":
            allRequestedBooks = requestedBookService.findAll(RequestedBookSortingOrder.DATE);
            break;
        default:
            allRequestedBooks = requestedBookService.findAll(RequestedBookSortingOrder.TITLE);
            break;
        }

        model.addAttribute("allRequestedBooks", allRequestedBooks);
        model.addAttribute("sort", sort);

        return "admin/requests_all";
    }

    @RequestMapping(value = "/admin/book/lend", method = RequestMethod.GET)
    public String adminChooseBookToLend(@RequestParam("id") int libraryUserId,
            @RequestParam(value = "available", required = false) Boolean available,
            @RequestParam(value = "book", required = false) Integer bookId, ModelMap model) {

        List<Book> books = bookService.findAllBooks();
        model.addAttribute("libraryUserId", libraryUserId);
        model.addAttribute("books", books);

        if (loanService.findByUserId(libraryUserId).size() >= 4) {
            model.addAttribute("message", "This user cannot borrow more books.");
            return "admin/book_lend";
        }

        if ((available != null) && (available == false)) {
            Book book = bookService.findBookById(bookId);
            model.addAttribute("message", "<strong>" + book.getTitle() + "</strong> is not available.");
        }

        return "admin/book_lend";
    }

    @RequestMapping(value = "/admin/book/lend", method = RequestMethod.POST)
    public String adminLendBook(@RequestParam("book_id") int bookId, @RequestParam("libraryuser_id") int libraryUserId,
            ModelMap model) {

        if (loanService.findByBookId(bookId) != null) {
            return "redirect:/admin/book/lend?id=" + String.valueOf(libraryUserId) + "&book="
                    + String.valueOf(bookId) + "&available=false";
        }

        Book book = bookService.findBookById(bookId);
        LibraryUser libraryUser = libraryUserService.findById(libraryUserId);

        RequestedBook requestedBook = requestedBookService.findByBookAndUser(bookId, libraryUserId);
        if (requestedBook != null) {
            requestedBookService.deleteRequestedBook(requestedBook);
        }

        LocalDate loanStart = LocalDate.now();
        LocalDate dueDate = loanStart.plusMonths(1);
        Loan loan = new Loan(book.getId(), book.getTitle(), libraryUser.getId(), libraryUser.getFullName(), loanStart,
                dueDate, false);
        loanService.saveLoan(loan);

        return "redirect:/admin/user?id=" + String.valueOf(libraryUserId);
    }

    @RequestMapping(value = "/admin/book/return", method = RequestMethod.GET)
    public String adminAskIfReturnBook(@RequestParam("id") int bookId, @RequestParam("user") int libraryUserId,
            ModelMap model) {

        Loan borrowedBook = loanService.findByBookAndUser(bookId, libraryUserId);
        model.addAttribute("borrowedBook", borrowedBook);

        return "admin/book_return";
    }

    @RequestMapping(value = "/admin/book/return", method = RequestMethod.POST)
    public String adminReturnBook(@RequestParam("book_id") int bookId, @RequestParam("user_id") int libraryUserId,
            ModelMap model) {

        Loan borrowedBook = loanService.findByBookAndUser(bookId, libraryUserId);
        String pathToRedirect = "/admin/user?id=" + borrowedBook.getUserId();

        loanService.deleteLoan(borrowedBook);

        return "redirect:" + pathToRedirect;
    }

    @RequestMapping(value = "/admin/book/renew", method = RequestMethod.GET)
    public String adminAskIfRenewBook(@RequestParam("id") int bookId, @RequestParam("user") int libraryUserId,
            ModelMap model) {

        Loan borrowedBook = loanService.findByBookAndUser(bookId, libraryUserId);
        model.addAttribute("borrowedBook", borrowedBook);

        if (borrowedBook.getRenewed().equals(true)) {
            model.addAttribute("message", "Book already renewed by this user.");
            return "admin/book_renew";
        }

        List<RequestedBook> requestedList = requestedBookService.findByBookId(bookId);
        if (!requestedList.isEmpty()) {
            if (requestedList.size() > 1) {
                model.addAttribute("message", "This book is requested by " + requestedList.size() + " users.");
            } else {
                model.addAttribute("message", "This book is requested by 1 user.");
            }
            return "admin/book_renew";
        }

        return "admin/book_renew";
    }

    @RequestMapping(value = "/admin/book/renew", method = RequestMethod.POST)
    public String adminRenewBook(@RequestParam("book_id") int bookId, @RequestParam("user_id") int libraryUserId,
            ModelMap model) {

        Loan borrowedBook = loanService.findByBookAndUser(bookId, libraryUserId);
        LocalDate newDueDate = LocalDate.now().plusMonths(1);
        borrowedBook.setRenewed(true);
        borrowedBook.setDueDate(newDueDate);
        loanService.renewBook(borrowedBook);

        return "redirect:/admin/user?id=" + borrowedBook.getUserId();
    }

    @RequestMapping(value = "/admin/user/enable", method = RequestMethod.GET)
    public String adminEnableAccount(@RequestParam("id") int libraryUserId, ModelMap model) {

        libraryUserService.changeStatus(libraryUserId, true);

        return "redirect:/admin/user/all";
    }

    @RequestMapping(value = "/admin/user/disable", method = RequestMethod.GET)
    public String adminDisableAccount(@RequestParam("id") int libraryUserId, ModelMap model) {

        libraryUserService.changeStatus(libraryUserId, false);

        return "redirect:/admin/user/all";
    }

    @RequestMapping(value = "/admin/book/overdue/all", method = RequestMethod.GET)
    public String adminAllOverdueBooks(@RequestParam(value = "sort", required = false) String sort,
            ModelMap model) {

        List<Loan> allOverdueBooks;
        if (sort == null) sort = "date";

        switch (sort) {
        case "title":
            allOverdueBooks = loanService.findAllOverdue(LoanSortingOrder.TITLE);
            break;
        case "user":
            allOverdueBooks = loanService.findAllOverdue(LoanSortingOrder.USER);
            break;
        case "date":
            allOverdueBooks = loanService.findAllOverdue(LoanSortingOrder.DATE);
            break;
        default:
            allOverdueBooks = loanService.findAllOverdue(LoanSortingOrder.TITLE);
            break;
        }

        model.addAttribute("allOverdueBooks", allOverdueBooks);
        model.addAttribute("sort", sort);

        return "admin/overdue_all";
    }

}
