package eu.rrozanski.librarymanager.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.rrozanski.librarymanager.dao.BookDao.IsbnType;
import eu.rrozanski.librarymanager.model.Author;
import eu.rrozanski.librarymanager.model.Book;
import eu.rrozanski.librarymanager.model.LibraryUser;
import eu.rrozanski.librarymanager.model.Loan;
import eu.rrozanski.librarymanager.model.RequestedBook;
import eu.rrozanski.librarymanager.model.SearchCriteria;
import eu.rrozanski.librarymanager.service.BookService;
import eu.rrozanski.librarymanager.service.LibraryUserService;
import eu.rrozanski.librarymanager.service.LoanService;
import eu.rrozanski.librarymanager.service.RequestedBookService;

@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    BookService bookService;

    @Autowired
    private LibraryUserService libraryUserService;

    @Autowired
    private RequestedBookService requestedBookService;

    @Autowired
    LoanService loanService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage() {
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            ModelMap model) {

        if (logout != null) {
            model.addAttribute("message",
                    "You've been logged out successfully.");
        }
        if (error != null) {
            model.addAttribute("error", "Invalid username and/or password.");
        }

        return "login";
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String userAccount(ModelMap model) {

        LibraryUser libraryUser = libraryUserService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        List<RequestedBook> requestedBooks = requestedBookService.findAllByUser(libraryUser.getId());
        List<Loan> borrowedBooks = loanService.findByUserId(libraryUser.getId());

        model.addAttribute("libraryUser", libraryUser);
        model.addAttribute("requestedBooks", requestedBooks);
        model.addAttribute("borrowedBooks", borrowedBooks);

        return "user_account";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String quickSearch(@RequestParam("title") String bookTitle,
            ModelMap model) {

        String message = null;
        List<Book> books = new ArrayList<Book>();

        if ((bookTitle.isEmpty()) || (bookTitle.length() < 3)) {
            message = "Title must be at least 3 characters long.";
        } else {
            books = bookService.findBooksByTitle(bookTitle);
            if (books.isEmpty()) {
                message = "Book not found.";
            }
        }

        model.addAttribute("message", message);
        model.addAttribute("books", books);

        return "quick_search";
    }

    @ModelAttribute("searchCategories")
    public Map<String, String> populateSearchCategories() {
        Map<String, String> searchCategories = new LinkedHashMap<String, String>();
        searchCategories.put("title", "Title");
        searchCategories.put("author", "Author");
        searchCategories.put("series", "Series");
        searchCategories.put("isbn", "ISBN");
        searchCategories.put("isbn13", "ISBN-13");
        return searchCategories;
    }

    @RequestMapping(value = "/search/advanced", method = RequestMethod.GET)
    public String advancedSearch(ModelMap model) {

        SearchCriteria searchCriteria = new SearchCriteria();
        model.addAttribute("searchCriteria", searchCriteria);

        return "advanced_search";
    }

    @RequestMapping(value = "/search/advanced", method = RequestMethod.POST)
    public String advancedSearchResults(
            @ModelAttribute("searchCriteria") SearchCriteria searchCriteria,
            BindingResult result, ModelMap model) {

        List<Book> books = new ArrayList<Book>();

        if (searchCriteria.getPhrase().isEmpty()) {
            model.addAttribute("message", "Search phrase is required.");
            return "advanced_search";
        }

        if (searchCriteria.getCategory().contentEquals("title")) {
            books = bookService.findBooksByTitle(searchCriteria.getPhrase());
        } else if (searchCriteria.getCategory().contentEquals("author")) {
            books = bookService.findByAuthorName(searchCriteria.getPhrase());
        } else if (searchCriteria.getCategory().contentEquals("series")) {
            books = bookService.findBySeries(searchCriteria.getPhrase());
        } else if (searchCriteria.getCategory().contentEquals("isbn")) {
            books = bookService.findByIsbn(IsbnType.ISBN10, searchCriteria.getPhrase());
        } else if (searchCriteria.getCategory().contentEquals("isbn13")) {
            books = bookService.findByIsbn(IsbnType.ISBN13, searchCriteria.getPhrase());
        }

        if (books.isEmpty()) {
            model.addAttribute("message", "Book not found.");
        } else {
            model.addAttribute("books", books);
        }

        return "advanced_search";
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public String bookDetails(@RequestParam("id") int bookId,
            ModelMap model) {

        Book book = bookService.findBookById(bookId);

        if (book != null) {
            List<String> sortedAuthorsNames = new ArrayList<String>();
            for (Author author : book.getAuthors()) {
                sortedAuthorsNames.add(author.getName());
            }
            Collections.sort(sortedAuthorsNames);
            model.addAttribute("book", book);
            model.addAttribute("sortedAuthorsNames", sortedAuthorsNames);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            LibraryUser libraryUser = libraryUserService
                    .findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            RequestedBook requestedBook = new RequestedBook(book.getId(), book.getTitle(), libraryUser.getId(),
                    libraryUser.getFullName(), LocalDate.now());

            if (loanService.findByBookAndUser(book.getId(), libraryUser.getId()) != null) {
                model.addAttribute("bookAlreadyBorrowed", true);
            }

            if (requestedBookService.checkIfBookRequested(book.getId(), libraryUser.getId())) {
                model.addAttribute("bookAlreadyRequested", true);
            }

            model.addAttribute("requestedBook", requestedBook);
            model.addAttribute("libraryUser", libraryUser);
        }

        return "book_details";
    }

    @RequestMapping(value = "/book/request/add", method = RequestMethod.POST)
    public String requestBook(@ModelAttribute("requestedBook") RequestedBook requestedBook,
            BindingResult result, ModelMap model) {

        requestedBookService.saveRequestedBook(requestedBook);

        return "redirect:/book?id=" + String.valueOf(requestedBook.getBookId());
    }

    @RequestMapping(value = "/book/request/remove", method = RequestMethod.POST)
    public String cancelBookRequest(@ModelAttribute("requestedBook") RequestedBook requestedBook,
            BindingResult result, ModelMap model) {

        requestedBookService.deleteRequestedBook(requestedBookService.findByBookAndUser(requestedBook.getBookId(),
                requestedBook.getUserId()));

        return "redirect:/book?id=" + String.valueOf(requestedBook.getBookId());
    }

}
