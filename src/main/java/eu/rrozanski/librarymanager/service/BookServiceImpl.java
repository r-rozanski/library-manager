package eu.rrozanski.librarymanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.BookDao;
import eu.rrozanski.librarymanager.dao.BookDao.IsbnType;
import eu.rrozanski.librarymanager.dao.LoanDao;
import eu.rrozanski.librarymanager.model.Book;

@Service("bookService")
@Transactional
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao dao;

    @Autowired
    private LoanDao loanDao;

    public void saveBook(Book book) {
        dao.saveBook(book);
    }

    public void updateBook(Book book) {
        dao.updateBook(book);
    }

    public void deleteBook(Book book) {
        dao.deleteBook(book);
    }

    public List<Book> findAllBooks() {
        return dao.findAllBooks();
    }

    public Book findBookById(int id) {
        return dao.findBookById(id);
    }

    public List<Book> findBooksByTitle(String title) {
        return dao.findBooksByTitle(title);
    }

    public List<Book> findByAuthorId(int authorId) {
        return dao.findByAuthorId(authorId);
    }

    public List<Book> findByAuthorName(String name) {
        return dao.findByAuthorName(name);
    }

    public List<Book> findBySeries(String series) {
        return dao.findBySeries(series);
    }

    public List<Book> findByIsbn(IsbnType type, String number) {
        return dao.findByIsbn(type, number);
    }

    public Boolean checkIfBookExists(String title) {
        return dao.checkIfBookExists(title);
    }

    public Long countAll() {
        return dao.countAll();
    }

    public Long countAvailable() {
        return (countAll() - loanDao.countAll());
    }

}
