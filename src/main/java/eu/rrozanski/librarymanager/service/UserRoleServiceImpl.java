package eu.rrozanski.librarymanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.UserRoleDao;
import eu.rrozanski.librarymanager.model.UserRole;

@Service("userRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao dao;

    public UserRole findByRoleName(String role) {
        return dao.findByRoleName(role);
    }

}
