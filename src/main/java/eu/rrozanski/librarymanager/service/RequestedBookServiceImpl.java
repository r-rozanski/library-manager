package eu.rrozanski.librarymanager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.RequestedBookDao;
import eu.rrozanski.librarymanager.dao.RequestedBookDao.RequestedBookSortingOrder;
import eu.rrozanski.librarymanager.model.RequestedBook;

@Service("requestedBookService")
@Transactional
public class RequestedBookServiceImpl implements RequestedBookService {

    @Autowired
    private RequestedBookDao dao;

    public void saveRequestedBook(RequestedBook requestedBook) {
        dao.saveRequestedBook(requestedBook);
    }

    public void deleteRequestedBook(RequestedBook requestedBook) {
        dao.deleteRequestedBook(requestedBook);
    }

    public boolean checkIfBookRequested(int bookId, int userId) {
        return dao.checkIfBookRequested(bookId, userId);
    }

    public RequestedBook findById(int id) {
        return dao.findById(id);
    }

    public RequestedBook findByBookAndUser(int bookId, int userId) {
        return dao.findByBookAndUser(bookId, userId);
    }

    public List<RequestedBook> findByBookId(int bookId) {
        return dao.findByBookId(bookId);
    }

    public List<RequestedBook> findAll(RequestedBookSortingOrder sortingOrder) {
        return dao.findAll(sortingOrder);
    }

    public List<RequestedBook> findAllByUser(int userId) {
        return dao.findAllByUser(userId);
    }

    public Integer countAll() {
        List<Integer> filteredRequestedBooks = new ArrayList<Integer>();
        for (RequestedBook requestedBook : findAll(RequestedBookSortingOrder.TITLE)) {
            if (!filteredRequestedBooks.contains(requestedBook.getBookId())) {
                filteredRequestedBooks.add(requestedBook.getBookId());
            }
        }
        return filteredRequestedBooks.size();
    }

}
