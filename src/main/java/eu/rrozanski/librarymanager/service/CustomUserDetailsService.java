package eu.rrozanski.librarymanager.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.LibraryUserDao;
import eu.rrozanski.librarymanager.model.UserRole;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private LibraryUserDao libraryUserDao;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        eu.rrozanski.librarymanager.model.LibraryUser libraryUser = libraryUserDao
                .findByUsername(username);

        List<GrantedAuthority> authorities = buildUserAuthority(libraryUser.getRoles());

        return buildUserForAuthentication(libraryUser, authorities);
    }

    private User buildUserForAuthentication(
            eu.rrozanski.librarymanager.model.LibraryUser libraryUser,
            List<GrantedAuthority> authorities) {

        return new User(libraryUser.getUsername(), libraryUser.getPassword(),
                true, true, true, true, authorities);

    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        for (UserRole userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
        }

        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(
                setAuths);

        return result;
    }

}
