package eu.rrozanski.librarymanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.LibraryUserDao;
import eu.rrozanski.librarymanager.model.LibraryUser;

@Service("libraryUserService")
@Transactional
public class LibraryUserServiceImpl implements LibraryUserService {

    @Autowired
    private LibraryUserDao dao;

    public void saveUser(LibraryUser libraryUser) {
        dao.saveUser(libraryUser);
    }

    public void updateUser(LibraryUser libraryUser) {
        dao.updateUser(libraryUser);
    }

    public void deleteUser(LibraryUser libraryUser) {
        dao.deleteUser(libraryUser);
    }

    public void changeStatus(int userId, boolean status) {
        dao.changeStatus(userId, status);
    }

    public List<LibraryUser> findAllUsers() {
        return dao.findAllUsers();
    }

    public LibraryUser findByUsername(String username) {
        return dao.findByUsername(username);
    }

    public LibraryUser findById(int id) {
        return dao.findById(id);
    }

    public Boolean checkIfUserExists(String username) {
        return dao.checkIfUserExists(username);
    }

    public Long countAll() {
        return dao.countAll();
    }

    public Long countActive() {
        return dao.countActive();
    }

    public Long countInactive() {
        return dao.countInactive();
    }

}
