package eu.rrozanski.librarymanager.service;

import java.util.List;

import eu.rrozanski.librarymanager.model.LibraryUser;

public interface LibraryUserService {

    public void saveUser(LibraryUser libraryUser);

    public void updateUser(LibraryUser libraryUser);

    public void deleteUser(LibraryUser libraryUser);

    void changeStatus(int userId, boolean status);

    public List<LibraryUser> findAllUsers();

    public LibraryUser findByUsername(String username);

    public LibraryUser findById(int id);

    public Boolean checkIfUserExists(String username);

    Long countAll();

    Long countActive();

    Long countInactive();

}
