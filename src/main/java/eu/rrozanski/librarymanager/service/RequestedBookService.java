package eu.rrozanski.librarymanager.service;

import java.util.List;

import eu.rrozanski.librarymanager.dao.RequestedBookDao.RequestedBookSortingOrder;
import eu.rrozanski.librarymanager.model.RequestedBook;

public interface RequestedBookService {

    void saveRequestedBook(RequestedBook requestedBook);

    void deleteRequestedBook(RequestedBook requestedBook);

    boolean checkIfBookRequested(int bookId, int userId);

    RequestedBook findById(int id);

    RequestedBook findByBookAndUser(int bookId, int userId);

    List<RequestedBook> findByBookId(int bookId);

    List<RequestedBook> findAll(RequestedBookSortingOrder sortingOrder);

    List<RequestedBook> findAllByUser(int userId);

    Integer countAll();

}
