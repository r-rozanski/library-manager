package eu.rrozanski.librarymanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.rrozanski.librarymanager.dao.AuthorDao;
import eu.rrozanski.librarymanager.model.Author;

@Service("authorService")
@Transactional
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDao dao;

    public void saveAuthor(Author author) {
        dao.saveAuthor(author);
    }

    public void updateAuthor(Author author) {
        dao.updateAuthor(author);
    }

    public void deleteAuthor(Author author) {
        dao.deleteAuthor(author);
    }

    public List<Author> findAllAuthors() {
        return dao.findAllAuthors();
    }

    public Author findAuthorById(int id) {
        return dao.findAuthorById(id);
    }

    public Boolean checkIfAuthorExists(String name) {
        return dao.checkIfAuthorExists(name);
    }

    public Long countAll() {
        return dao.countAll();
    }

}
