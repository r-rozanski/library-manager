package eu.rrozanski.librarymanager.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.rrozanski.librarymanager.dao.LoanDao;
import eu.rrozanski.librarymanager.dao.LoanDao.LoanSortingOrder;
import eu.rrozanski.librarymanager.model.Loan;

@Service("loanService")
@Transactional
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanDao dao;

    public void saveLoan(Loan loan) {
        dao.saveLoan(loan);
    }

    public void deleteLoan(Loan loan) {
        dao.deleteLoan(loan);
    }

    public void renewBook(Loan loan) {
        dao.renewBook(loan);
    }

    public Loan findById(int id) {
        return dao.findById(id);
    }

    public Loan findByBookId(int bookId) {
        return dao.findByBookId(bookId);
    }

    public List<Loan> findByUserId(int userId) {
        return dao.findByUserId(userId);
    }

    public Loan findByBookAndUser(int bookId, int userId) {
        return dao.findByBookAndUser(bookId, userId);
    }

    public List<Loan> findAll() {
        return dao.findAll();
    }

    public List<Loan> findAllOverdue(LoanSortingOrder sortingOrder) {
        return dao.findAllOverdue(sortingOrder);
    }

    public Long countAll() {
        return dao.countAll();
    }

    public Long countOverdue() {
        return dao.countOverdue();
    }

}
