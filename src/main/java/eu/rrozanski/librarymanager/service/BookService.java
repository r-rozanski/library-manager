package eu.rrozanski.librarymanager.service;

import java.util.List;

import eu.rrozanski.librarymanager.dao.BookDao.IsbnType;
import eu.rrozanski.librarymanager.model.Book;

public interface BookService {

    public void saveBook(Book book);

    public void updateBook(Book book);

    public void deleteBook(Book book);

    public List<Book> findAllBooks();

    public Book findBookById(int id);

    public List<Book> findBooksByTitle(String title);

    public List<Book> findByAuthorId(int authorId);

    public List<Book> findByAuthorName(String name);

    public List<Book> findBySeries(String series);

    public List<Book> findByIsbn(IsbnType type, String number);

    public Boolean checkIfBookExists(String title);

    Long countAll();

    Long countAvailable();

}
