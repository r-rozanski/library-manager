package eu.rrozanski.librarymanager.service;

import java.util.List;

import eu.rrozanski.librarymanager.dao.LoanDao.LoanSortingOrder;
import eu.rrozanski.librarymanager.model.Loan;

public interface LoanService {

    void saveLoan(Loan loan);

    void deleteLoan(Loan loan);

    void renewBook(Loan loan);

    Loan findById(int id);

    Loan findByBookId(int bookId);

    List<Loan> findByUserId(int userId);

    Loan findByBookAndUser(int bookId, int userId);

    List<Loan> findAll();

    List<Loan> findAllOverdue(LoanSortingOrder sortingOrder);

    Long countAll();

    Long countOverdue();

}
