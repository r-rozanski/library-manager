package eu.rrozanski.librarymanager.service;

import eu.rrozanski.librarymanager.model.UserRole;

public interface UserRoleService {

    public UserRole findByRoleName(String role);

}
