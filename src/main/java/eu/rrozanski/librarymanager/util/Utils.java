package eu.rrozanski.librarymanager.util;

import java.util.Collection;

public class Utils {

    public static boolean contains(Collection<Object> collection, Object item) {
        if ((collection == null) || (item == null)) {
            return false;
        }
        return collection.contains(item);
    }

}
