package eu.rrozanski.librarymanager.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import eu.rrozanski.librarymanager.model.Author;
import eu.rrozanski.librarymanager.service.AuthorService;

@Component
public class AuthorToSetConverter implements Converter<String, Set<Author>> {

    @Autowired
    private AuthorService authorService;

    @Override
    public Set<Author> convert(String authorId) {
        Set<Author> authors = new HashSet<Author>();
        authors.add(authorService.findAuthorById(Integer.parseInt(authorId)));
        return authors;
    }

}
