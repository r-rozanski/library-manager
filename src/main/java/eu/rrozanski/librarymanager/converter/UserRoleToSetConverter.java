package eu.rrozanski.librarymanager.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import eu.rrozanski.librarymanager.model.UserRole;
import eu.rrozanski.librarymanager.service.UserRoleService;

@Component
public class UserRoleToSetConverter implements Converter<String, Set<UserRole>> {

    @Autowired
    private UserRoleService userRoleService;

    @Override
    public Set<UserRole> convert(String role) {
        Set<UserRole> roles = new HashSet<UserRole>();
        roles.add(userRoleService.findByRoleName(role));
        return roles;
    }

}
