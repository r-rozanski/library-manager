package eu.rrozanski.librarymanager.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "title" }) })
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(name = "title", length = 100, nullable = false)
    private String title;

    @NotNull
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "author_book",
        joinColumns = @JoinColumn(name = "book_id"),
        inverseJoinColumns = @JoinColumn(name = "author_id"))
    private Set<Author> authors;

    @Transient
    private String sortedAuthors;

    @Size(max = 50)
    @Column(length = 50)
    private String series;

    @Min(1)
    @Column(name = "series_part")
    private Integer seriesPart;

    @NotNull
    @Size(min = 3, max = 20)
    @Column(length = 20, nullable = false)
    private String genre;

    @NotNull
    @Size(min = 3, max = 20)
    @Column(length = 20, nullable = false)
    private String language;

    @NotNull
    @Min(1)
    @Column(nullable = false)
    private Integer pages;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(length = 50, nullable = false)
    private String publisher;

    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @Column(name = "publication_date", nullable = false)
    private LocalDate publicationDate;

    @NotNull
    @Pattern(regexp = "[0-9]{10}")
    @Column(length = 10, nullable = false)
    private String isbn;

    @NotNull
    @Pattern(regexp = "[0-9]{13}")
    @Column(length = 13, nullable = false)
    private String isbn13;

    @NotNull
    @Size(min = 3, max = 3000)
    @Column(length = 3000, nullable = false)
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getSortedAuthors() {
        sortedAuthors = "";
        List<String> authorsNames = new ArrayList<String>();

        for (Author author : authors) {
            authorsNames.add(author.getName());
        }
        Collections.sort(authorsNames);

        return StringUtils.join(authorsNames, ", ");
    }

    public void setSortedAuthors(String sortedAuthors) {
        this.sortedAuthors = sortedAuthors;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getSeriesPart() {
        return seriesPart;
    }

    public void setSeriesPart(Integer seriesPart) {
        this.seriesPart = seriesPart;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
