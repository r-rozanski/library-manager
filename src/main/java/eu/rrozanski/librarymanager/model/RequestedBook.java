package eu.rrozanski.librarymanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "book_id", "user_id" }) })
public class RequestedBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "book_id", nullable = false)
    private int bookId;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(name = "book_title", length = 100, nullable = false)
    private String bookTitle;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private int userId;

    @NotNull
    @Size(min = 6, max = 100)
    @Column(name = "user_fullname", length = 100, nullable = false)
    private String userFullName;

    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @Column(name = "request_date", nullable = false)
    private LocalDate requestDate;

    public RequestedBook() {

    }

    public RequestedBook(int bookId, String bookTitle, int userId, String userFullName, LocalDate requestDate) {
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.userId = userId;
        this.userFullName = userFullName;
        this.requestDate = requestDate;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

}
