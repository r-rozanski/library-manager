package eu.rrozanski.librarymanager.model;

public class SearchCriteria {

    private String phrase;
    private String category;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
