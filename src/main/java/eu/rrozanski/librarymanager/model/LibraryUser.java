package eu.rrozanski.librarymanager.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "username" })})
public class LibraryUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(nullable = false, length = 30)
    private String username;

    @NotNull
    @Size(min = 3, max = 60)
    @Column(nullable = false, length = 60)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "libraryuser_userrole",
        joinColumns = @JoinColumn(name = "libraryuser_id"),
        inverseJoinColumns = @JoinColumn(name = "userrole_id"))
    private Set<UserRole> roles;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "first_name", nullable = false, length = 50)
    private String firstName;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "last_name", nullable = false, length = 50)
    private String lastName;

    @Transient
    private String fullName;

    @NotNull
    @Size(min = 7, max = 30)
    @Column(name = "phone", nullable = false, length = 30)
    private String phone;

    @NotNull
    @Pattern(regexp = "^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$")
    @Column(nullable = false)
    private String email;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(nullable = false, length = 100)
    private String address;

    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @Column(name = "registration_date", nullable = false)
    private LocalDate registrationDate;

    @Type(type = "true_false")
    private boolean active;

    @Transient
    private String formattedRoles;

    @Transient
    private boolean admin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getFormattedRoles() {
        List<String> userRoles = new ArrayList<String>();

        for (UserRole role : roles) {
            userRoles.add(StringUtils.capitalize(StringUtils
                    .lowerCase(StringUtils.remove(role.getRole(), "ROLE_"))));
        }
        Collections.sort(userRoles);

        return StringUtils.join(userRoles, ", ");
    }

    public void setFormattedRoles(String formattedRoles) {
        this.formattedRoles = formattedRoles;
    }

    public boolean isAdmin() {
        boolean admin = false;

        for (UserRole role : roles) {
            if (role.getRole().contains("ADMIN")) admin = true;
        }

        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

}
