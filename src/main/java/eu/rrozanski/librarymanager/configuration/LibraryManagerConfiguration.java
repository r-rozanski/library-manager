package eu.rrozanski.librarymanager.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import eu.rrozanski.librarymanager.converter.AuthorToSetConverter;
import eu.rrozanski.librarymanager.converter.AuthorsToSetConverter;
import eu.rrozanski.librarymanager.converter.UserRoleToSetConverter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "eu.rrozanski.librarymanager")
@Import({ SecurityConfiguration.class })
public class LibraryManagerConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private AuthorToSetConverter authorToSetConverter;

    @Autowired
    private AuthorsToSetConverter authorsToSetConverter;

    @Autowired
    private UserRoleToSetConverter userRoleToSetConverter;

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(authorToSetConverter);
        registry.addConverter(authorsToSetConverter);
        registry.addConverter(userRoleToSetConverter);
    };

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        return messageSource;
    }
}
