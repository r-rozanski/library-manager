-- Clear data in all tables
TRUNCATE author_book, author, book;
TRUNCATE libraryuser_userrole, libraryuser, userrole;
TRUNCATE loan, requestedbook;


-- Create authors 
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (1, 'Frank Herbert', '1920-10-08', '1986-02-11');
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (2, 'Terry Pratchett', '1948-04-28', '2015-03-12');
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (3, 'Arthur C. Clarke', '1917-12-16', '2008-03-18');
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (4, 'Isaac Asimov', '1920-01-02', '1992-04-06');
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (5, 'James Rollins', '1961-08-20', null);
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (6, 'Bill Bryson', '1951-12-08', null);
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (7, 'Nigel Findley', '1959-07-22', '1995-02-19');
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (8, 'Clive Cussler', '1931-07-15', null);
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (9, 'Brian Herbert', '1947-06-29', null);
INSERT INTO author (id, name, date_of_birth, date_of_death) VALUES (10, 'Kevin J. Anderson', '1962-03-27', null);


-- Create books
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (1, 'Dune', 'Dune Chronicles', 1, 'Science Fiction', 'English', 604, 'Hodder & Stoughton', '2006-06-01', '0340839937', '9780340839935', 'Set in the far future amidst a sprawling feudal interstellar empire where planetary dynasties are controlled by noble houses that owe an allegiance to the imperial House Corrino, Dune tells the story of young Paul Atreides (the heir apparent to Duke Leto Atreides and heir of House Atreides) as he and his family accept control of the desert planet Arrakis, the only source of the "spice" melange, the most important and valuable substance in the cosmos. The story explores the complex, multi-layered interactions of politics, religion, ecology, technology, and human emotion as the forces of the empire confront each other for control of Arrakis.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (2, 'Dune Messiah', 'Dune Chronicles', 2, 'Science Fiction', 'English', 331, 'Ace Books', '1987-07-15', '0441172695', '9780441172696', 'Dune Messiah continues the story of the man Muad''dib, heir to a power unimaginable, bringing to completion the centuries-old scheme to create a super-being.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (3, 'Guards! Guards!', 'Discworld', 8, 'Fantasy', 'English', 355, 'HarperCollins', '2001-07-31', '0061020648', '9780061020643', 'Here there be dragons... and the denizens of Ankh-Morpork wish one huge firebreather would return from whence it came. Long believed extinct, a superb specimen of draco nobilis has appeared in Discworld''s greatest city. Not only does this unwelcome visitor have a nasty habit of charbroiling everything in its path, in rather short order it is crowned King (it is a noble dragon, after all...).');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (4, 'Nation', null, null, 'Fantasy', 'English', 367, 'HarperCollins', '2008-09-30', '0061433012', '9780061433016', 'Alone on a desert island - everything and everyone he knows and loves has been washed away in a storm - Mau is the last surviving member of his nation. He''s completely alone - or so he thinks until he finds the ghost girl. She has no toes, wears strange lacy trousers like the grandfather bird, and gives him a stick that can make fire.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (5, 'Rendezvous with Rama', 'Rama', 1, 'Science Fiction', 'English', 243, 'Spectra', '1990-11-01', '0553287893', '9780553287899', 'The enigmatic object christened Rama was detected while still outside the orbit of Jupiter, and a first radar contact at such a distance was unprecedented, indicating that Rama was of exceptional size. As it raced through the Solar System, it became apparent that Rama was a cylinder so geometrically perfect that it might have been turned on a gigantic lathe. Mankind was about to receive its first vistor from the stars.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (6, 'Foundation', 'Foundation', 1, 'Science Fiction', 'English', 256, 'Spectra', '2004-06-01', '0553803719', '9780553803716', 'For twelve thousand years the Galactic Empire has ruled supreme. Now it is dying. But only Hari Seldon, creator of the revolutionary science of psychohistory, can see into the future - to a dark age of ignorance, barbarism, and warfare that will last thirty thousand years. To preserve knowledge and save mankind, Seldon gathers the best minds in the Empire - both scientists and scholars - and brings them to a bleak planet at the edge of the Galaxy to serve as a beacon of hope for a future generations. He calls his sanctuary the Foundation.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (7, 'The Judas Strain', 'Sigma Force', 4, 'Thriller', 'English', 450, 'William Morrow', '2008-01-11', '0060763892', '9780060763893', 'A judas strain is an organism that drives a species to extinction. From the Indian Ocean depths, an ancient menace plagues the modern world. Hope hides "within the language of angels". Aboard a cruise liner cum makeshift hospital, Dr Lisa Cummings and Monk Kokkalis, SIGMA agents, seek answers. But terrorists hijack the mercy ship for a floating bio-weapons lab.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (8, 'Notes from a Small Island', null, null, 'Non Fiction', 'English', 324, 'William Morrow Paperbacks', '1997-05-28', '0380727501', '9780380727506', 'After nearly two decades spent on British soil, Bill Bryson - bestselling author of "The Mother Tongue" and "Made in America" - decided to return to the United States. But before departing, he set out on a grand farewell tour of the green and kindly island that had so long been his home.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (9, 'Into the Void', 'Spelljammer: The Cloakmaster Cycle', 2, 'Fantasy', 'English', 311, 'TSR', '1991-09-01', '1560761547', '9781560761549', 'Plunged into a sea of alien faces, Teldin Moore isn''t sure whom to trust. His gnomish sidewheeler is attacked by space pirates, and Teldin is saved by a hideous mind flayer who offers to help the human use his magic cloak - but for whose gain? Teldin learns the basics of spelljamming, and he seeks an ancient arcne, one who might tell him more.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (10, 'The Sea Hunters', 'The Sea Hunters', 1, 'Adventure', 'English', 432, 'Pocket Star Books', '2003-08-26', '0743480694', '9780743480697', 'Clive Cussler and his crack team of NUMA (National Underwater Marine Agency, a nonprofit organization that searches for historic shipwrecks) volunteers have found the remains of these and numerous other tragic wrecks. Here are the dramatic, true accounts of twelve of the most remarkable underwater discoveries made by Cussler and his team. As suspenseful and satisfying as the best of his Dirk Pitt novels, "The Sea Hunters" is a unique story of true commitment and courage.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (11, 'The Butlerian Jihad', 'Legends of Dune', 1, 'Science Fiction', 'English', 684, 'Tor Books', '2003-09-15', '0765340771', '9780765340771', 'Decades after Herbert''s original novels, the Dune saga was continued by Frank Herbert''s son, Brian Herbert, in collaboration with Kevin J. Anderson. Working from Frank Herbert''s own notes, the acclaimed authors reveal the chapter of the Dune saga most eagerly anticipated by readers: the Butlerian Jihad.');
INSERT INTO book (id, title, series, series_part, genre, language, pages, publisher, publication_date, isbn, isbn13, description) VALUES (12, 'Dreamer of Dune', null, null, 'Biography', 'English', 592, 'Tor Books', '2004-07-01', '0765306476', '9780765306470', 'Brian Herbert, Frank Herbert''s eldest son, tells the provocative story of his father''s extraordinary life in this honest and loving chronicle. He has also brought to light all the events in Herbert''s life that would find their way into speculative fiction''s greatest epic.');


-- Create associations between books and authors
INSERT INTO author_book (book_id, author_id) VALUES (1, 1);
INSERT INTO author_book (book_id, author_id) VALUES (2, 1);
INSERT INTO author_book (book_id, author_id) VALUES (3, 2);
INSERT INTO author_book (book_id, author_id) VALUES (4, 2);
INSERT INTO author_book (book_id, author_id) VALUES (5, 3);
INSERT INTO author_book (book_id, author_id) VALUES (6, 4);
INSERT INTO author_book (book_id, author_id) VALUES (7, 5);
INSERT INTO author_book (book_id, author_id) VALUES (8, 6);
INSERT INTO author_book (book_id, author_id) VALUES (9, 7);
INSERT INTO author_book (book_id, author_id) VALUES (10, 8);
INSERT INTO author_book (book_id, author_id) VALUES (11, 9);
INSERT INTO author_book (book_id, author_id) VALUES (11, 10);
INSERT INTO author_book (book_id, author_id) VALUES (12, 9);


-- Create users
-- User 'admin' - password: 'admin'
INSERT INTO libraryuser (id, username, password, first_name, last_name, phone, email, address, registration_date, active) VALUES (1, 'admin', '$2a$08$5U3YU9j6a7Pb6TajEN7aUeQgHtAf.L5Y7Rar.Hm5OvAHe1r7HQrsm', 'Robert', 'Wright', '5-(251)502-9252', 'john.doe@gmail.com', '15242 Goodland Parkway', '2015-01-01', 'T');
-- User 'radek' - password: 'password'
INSERT INTO libraryuser (id, username, password, first_name, last_name, phone, email, address, registration_date, active) VALUES (2, 'radek', '$2a$08$AiCaaegIAspqjtt34CwVg.Ka9pDEobkzrpGZEOs4xoH9AbwNzZiju', 'Radoslaw', 'Rozanski', '6-(851)020-9086', 'r.rozanski@gmail.com', '3146 Utah Drive', '2015-04-10', 'T');
-- User 'alice' - password: 'password'
INSERT INTO libraryuser (id, username, password, first_name, last_name, phone, email, address, registration_date, active) VALUES (3, 'alice', '$2a$08$AiCaaegIAspqjtt34CwVg.Ka9pDEobkzrpGZEOs4xoH9AbwNzZiju', 'Alice', 'Sanders', '8-(080)646-0291', 'alice.sanders@gmail.com', '3949 Talisman Avenue', '2015-04-25', 'T');


-- Create roles
INSERT INTO userrole (id, role) VALUES (1, 'ROLE_ADMIN');
INSERT INTO userrole (id, role) VALUES (2, 'ROLE_USER');


-- Create loans
INSERT INTO loan (id, book_id, book_title, user_id, user_fullname, loan_start, due_date, renewed) VALUES (1, 12, 'Dreamer of Dune', 2, 'Radoslaw Rozanski', '2015-04-21', '2015-05-21', FALSE);
INSERT INTO loan (id, book_id, book_title, user_id, user_fullname, loan_start, due_date, renewed) VALUES (2, 7, 'The Judas Strain', 3, 'Alice Sanders', '2015-04-18', '2015-05-18', FALSE);
INSERT INTO loan (id, book_id, book_title, user_id, user_fullname, loan_start, due_date, renewed) VALUES (3, 9, 'Into the Void', 3, 'Alice Sanders', '2015-03-29', '2015-05-18', TRUE);


-- Create requests
INSERT INTO requestedbook (id, book_id, book_title, user_id, user_fullname, request_date) VALUES (1, 10, 'The Sea Hunters', 2, 'Radoslaw Rozanski', '2014-12-14');
INSERT INTO requestedbook (id, book_id, book_title, user_id, user_fullname, request_date) VALUES (2, 5, 'Rendezvous with Rama', 2, 'Radoslaw Rozanski', '2015-03-27');
INSERT INTO requestedbook (id, book_id, book_title, user_id, user_fullname, request_date) VALUES (3, 6, 'Foundation', 3, 'Alice Sanders', '2015-05-02');


-- Create associations between users and roles
INSERT INTO libraryuser_userrole (libraryuser_id, userrole_id) VALUES (1, 1);
INSERT INTO libraryuser_userrole (libraryuser_id, userrole_id) VALUES (1, 2);
INSERT INTO libraryuser_userrole (libraryuser_id, userrole_id) VALUES (2, 2);
INSERT INTO libraryuser_userrole (libraryuser_id, userrole_id) VALUES (3, 2);